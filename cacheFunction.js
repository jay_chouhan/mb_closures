const cacheFunction = cb => {
  let cache = {};
  return (...args) => {
    if (typeof cb === undefined) return null;
    if (cache[args] != undefined) {
      return cache[args];
    }
    let res = cb(...args);
    cache[args] = res;
    return res;
  };
};

module.exports = cacheFunction;
