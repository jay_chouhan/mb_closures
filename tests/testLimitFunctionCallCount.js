const limitFunctionCallCount = require("../limitFunctionCallCount");

const callback = num => num * num;
const result = limitFunctionCallCount(callback, 3);

// test case: callback only executes 3 times

console.log(result(1)); // 1
console.log(result(2)); // 4
console.log(result(3)); // 9
console.log(result(4)); // null
