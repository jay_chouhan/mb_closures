const cacheFunction = require("../cacheFunction");

const cb = (num, num2) => {
  console.log("this message only appears once in console");
  return num * num2;
};

result = cacheFunction(cb);

//test case: the console.log message in cb should appear only once.

console.log(result(2, 3)); //'this message only appears once in console' 3
console.log(result(2, 3)); //3
console.log(result(3, 4));
console.log(result(3, 4));
console.log(result(2, 4));
