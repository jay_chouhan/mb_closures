const counterFactory = require("../counterFactory");

result = counterFactory();

/* test case:
    a. result.increment() should increment counter by 1
    b. result.decrement() should decrement counter by 1
*/

console.log(result.increment()); // 1
console.log(result.decrement()); // 0
