// Should return a function that invokes `cb`.
// The returned function should only allow `cb` to be invoked `n` times.
// Returning null is acceptable if cb can't be returned

const limitFunctionCallCount = (callback, limit) => {
  let counter = 0;

  return (...parameter) => {
    counter++;
    if (
      callback === undefined ||
      typeof limit !== "number" ||
      counter > limit
    ) {
      return null;
    }
    return callback(...parameter);
  };
};

module.exports = limitFunctionCallCount;
